import React from "react";
import { Vector, Shape, Star } from '../../../Assets/Image/image'

const Product = ({ product }) => {
  
  return (
    <div className="main">
      <img
        className="product__image"
        src={product?.image}
        alt={product?.title}
      />
      <div className="product_name">
        <p>{product.name}</p>
      </div>
      <div style={{ height: '20px', marginTop: '10px' }}>
        <img src={Star} alt="Star" />
        <img src={Star} alt="Star" />
        <img src={Star} alt="Star" />
        <img src={Star} alt="Star" />
        <img src={Star} alt="Star" />
      </div>
      <div>
        <p style={{ textDecorationLine: 'line-through', height: '19px', opacity: '0.6' }}>{product.old_price}</p>
        <strong style={{ fontSize: '20px' }}>{product.price}</strong>
        <p style={{ fontSize: '14px', height: '17px', opacity: '0.6' }}>{product.monthly_pay}</p>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between", alignItems: 'center' }}>
        <button className="button">В корзину</button>
        <div style={{
          display: "flex", justifyContent: 'space-around'
          , width: "70px", height: '20px'
        }}>
          <img src={Vector} alt="Vector" />
          <img src={Shape} alt="Shape" />
        </div >
      </div>
    </div>
  );
};

export default Product;
