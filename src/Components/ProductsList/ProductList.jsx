import React from "react";
import "./productsList.css";
import Product from "./Product/Product";

const ProductList = ({ title, products }) => {
  
    
  return (
    <div style={{ margin: '50px' }}>
      <div id="click">
        <h1>{title}</h1>
      </div>
      <div className="product" style={{ gap: "18px", flexWrap: "wrap" }}>
        {products &&
          products?.map((product) => (
            <Product key={product.id} product={product} />
          ))}
        
      </div>
    </div>
  );
};

export default ProductList;
