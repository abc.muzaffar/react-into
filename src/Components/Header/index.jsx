import React from 'react';
import './header.css'

const Header = () => {
    return (
        <div className="flex header">
            
            <div className='flex'>
                <li>
                    <a href="#About">Акции и скидки</a>
                </li>
                <li>
                    <a href="#About">Ноутбуки и компьютеры</a>
                </li>
                <li>
                    <a href="#About">Смартфоны и гаджеты</a>
                </li>
                <li>
                    <a href="#About">Телевизоры и аудио</a>
                </li>
                <li>
                    <a href="#About">Красота и здоровье</a>
                </li>
                <li>
                    <a href="#About">Техника для дома</a>
                </li>
            </div>
        </div>
    );
}
 
export default Header;

