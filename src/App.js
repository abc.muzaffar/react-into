import React, { useState, useEffect} from "react";
import "./App.css";
import Header from "./Components/Header";
import ProductList from "./Components/ProductsList/ProductList";
import axios from 'axios';

const App = () => {

  const [products, setProducts] = useState([])
  
  const getProducts = () => {
    axios.get('https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products')
    .then(res => {
     
      setProducts(res.data)
    })
  }

  useEffect(() => {
    getProducts()
  }, [])
 
  return (
    <>
      <Header />
      <ProductList title={"Новинки"} products={products.filter(item => item.status === 'new')} />
      <ProductList title={"Выбор покупателей"} products={products} />
    </>
  )
}
export default App;
